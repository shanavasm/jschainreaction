/*********************************************************************
 * Copyright (C) 2014 Shanavas m <shanavas[dot]m2[at]gmail.com>
 *
 * This script is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 2 of the License, or (at your option) any later
 * version. See http://www.gnu.org/copyleft/gpl.html for the full text of the
 * license.
 ********************************************************************/

var ballCount = null; //Two dimensional array which stored the ball count.
var player = ''; //Next player
var divClass = null; // array which stores class of each division
//grid and ball colours for 8 players
var allPlayers = ["red","blue","green","yellow","violet","cyan","orange","white"];
//ball classes
var ballClass = ["one","two","three","four"];
var noOfPlayers = 0; //number of players
var Frame = null;
//debug
var debug = null;
var css3 = false;

var animation = [];
//browser compatibility
var pfx = ["webkit", "moz", "MS", "o", ""];
function PrefixedEvent(element, type, callback) {
    for (var p = 0; p < pfx.length; p++) {
	if (!pfx[p]) type = type.toLowerCase();
	element.addEventListener(pfx[p]+type, callback, false);
    }
}

function banner(){
    Frame = document.getElementById("frame");
    Frame.innerHTML = "<h2>Start Game</h2>" +
	"<select id=\"noOfPlayers\">"+
	"<option value=\"1\">Single Player</option>"+
	"<option value=\"2\">2 Players</option>"+
	"<option value=\"3\">3 Players</option>"+
	"<option value=\"4\">4 Players</option>"+
	"<option value=\"5\">5 Players</option>"+
	"<option value=\"6\">6 Players</option>"+
	"<option value=\"7\">7 Players</option>"+
	"<option value=\"8\">8 Players</option>"+
	"</select>&nbsp;&nbsp;&nbsp;"+
	"<input type=\"button\" value=\"Go\" onclick=\"init()\"/>";

    footer = document.getElementById("footer");
    footer.innerHTML = "Copyright &copy; 2014 shanavas m <br/>Suggestions and Feedbacks at shanavas.m2[at]gmail.com</div>";
    // check whether browser supports css3 animations
    if(Frame.style.transform === "" || Frame.style.msTransform === "" || Frame.style.WebkitTransform === ""){
	css3 = true; 
    }

}
function init(noPlayers){
    noOfPlayers = document.getElementById("noOfPlayers").value;
    auto = (noOfPlayers==1)?true:false;  //set auto mod for single player
    if(auto)
	noOfPlayers++; //to count computer as a player
    players = allPlayers.splice(0,noOfPlayers);
    Frame.innerHTML='';
    divWidth = '12.5';
    divHeight = '12.5';
    ballCount = new Array();
    divClass = new Array();
    player = 'red'; //default player
    Frame.className = 'red';

    //debug
    debug = document.getElementById("debug");

    //creates 8 x 8 grid and initialize ballCount as 0 and className as default
    var newDiv = null;
    for(var row=0;row<8;row++){
	ballCount[row] = new Array();
	divClass[row] = new Array();
	for(var col = 0; col<8; col++){
	    newDiv = document.createElement('DIV');
	    // newDiv.id = 'div' + row + col;
	    newDiv.id = row +''+ col;
	    newDiv.className = 'default';
	    newDiv.style.width = divWidth + "%";
	    newDiv.style.height = divHeight +"%";
	    newDiv.style.position = "absolute";
	    newDiv.style.left = col * divWidth +"%";
	    newDiv.style.top = row * divHeight + "%";
	    newDiv.setAttribute('onClick', "action("+row+","+col+");");
	    //atach animation end event handler
	    if(css3){
		PrefixedEvent(newDiv, "AnimationEnd", resetCell);
	    }
	    //create and append balls
	    for(var n in ballClass){
		var ball = document.createElement("DIV");
		ball.className = "ball "+ ballClass[n];
		newDiv.appendChild(ball);
	    }
	    //create 3D grid
	    if(css3){
		var leftwall = document.createElement("DIV");
		leftwall.className = "leftwall";
		newDiv.appendChild(leftwall);
		var topwall = document.createElement("DIV");
		topwall.className = "topwall";
		newDiv.appendChild(topwall);
		
		if(row == 7){
		    var bottomwall = document.createElement("DIV");
		    bottomwall.className = "bottomwall";
		    newDiv.appendChild(bottomwall);
		}
		if(col == 7){
		    var rightwall = document.createElement("DIV");
		    rightwall.className = "rightwall";
		    newDiv.appendChild(rightwall);
		}
	    }
	    
	    Frame.appendChild(newDiv);
	    ballCount[row][col] = 0;
	    divClass[row][col] = 'default';

	}
    }
}

//function on clicking a grid
function action(row, col){
    if(divClass[row][col] == 'default' || divClass[row][col] == player){
	reaction(row, col);
	gameOver();

	if(!animation.length){
	    Frame.className = toggle(Frame.className);
	    player = toggle(player);
	}
	
	//auto mode
	if(auto && (player == 'blue')){
	    do{
		auto_row = Math.floor(Math.random()*8);
		auto_col = Math.floor(Math.random()*8);
	    }
	    while(divClass[auto_row][auto_col] == 'red');
	    action(auto_row, auto_col);
	}
	
    }
}

function toggle(arg){
    var currentPlayer = players.indexOf(arg);
    var nextPlayer = players[(currentPlayer+1)%noOfPlayers];
    return nextPlayer;
}

function gameOver(){
    var countBalls = new Array();
    var totalBalls = 0;
    for(var ball in players){
	countBalls[players[ball]] = 0;
    }

    for(var row=0; row<8; row++){
	for(var col=0; col<8; col++){
	    if(divClass[row][col] != "default"){
		countBalls[divClass[row][col]] += ballCount[row][col];
		totalBalls += ballCount[row][col];
	    }
	}
    }

    if(totalBalls > noOfPlayers){
	for(var i=0; i<players.length; i++){
	    if(countBalls[players[i]] == 0){
		players.splice(i,1);
		noOfPlayers--;
		i--;
	    }

	}
	if(players.length == 1){
	    alert("Congrats\n"+players[0]+' wins');
	    banner();
	    return;

	}
    }

}
function reaction(row, col){
    var limit = 3;
    if(row == 0 || row == 7){
	limit--;
    }
    if(col == 0 || col == 7){
	limit--;
    }
    if(ballCount[row][col] == limit){
	//animate if browser supports
	if(css3){
	    animation.push(row*8 + col);
	    split(row, col, player);
	}else{
	    	document.getElementById(row+''+col).className = "default";
	    divClass[row][col] = 'default';
	    ballCount[row][col] = 0;
	    if(row != 0){
		reaction(row-1, col);
	    }
	    if(row != 7){
		reaction(row+1, col);
	    }
	    if(col !=0){
		reaction(row, col-1);
	    }
	    if(col != 7){
		reaction(row, col+1);
	    }
	}
	
    }else{
	ballCount[row][col] +=1;
	divClass[row][col] = player;
	document.getElementById(row+''+col).className = player+ " " +ballClass[ballCount[row][col] - 1];
    }
}

function split(row, col, player){
    cell = document.getElementById(row +''+ col);

    var className = player + " split ";
    if(row == 0){
	className += "top ";
    }else if(row == 7){
	className += "down ";
    }
    
    if(col == 0){
	className += "left";
    }else if(col == 7){
	className += "right";
    }
    cell.className = className;
}

function resetCell(e){

    row = Number(this.id[0]);
    col = Number(this.id[1]);
    if(animation.indexOf(row*8+col) != -1){
	animation.splice(animation.indexOf(row*8+col),1);
	document.getElementById(row+''+col).className = "default";
	divClass[row][col] = 'default';
	ballCount[row][col] = 0;
	if(row != 0){
	    reaction(row-1, col);
	}
	if(row != 7){
	    reaction(row+1, col);
	}
	if(col !=0){
	    reaction(row, col-1);
	}
	if(col != 7){
	    reaction(row, col+1);
	}

	if(!animation.length){
	    gameOver();
	    Frame.className = toggle(Frame.className);
	    player = toggle(player);
	    if(auto && player =="blue"){
		do{
		    auto_row = Math.floor(Math.random()*8);
		    auto_col = Math.floor(Math.random()*8);
		}
		while(divClass[auto_row][auto_col] == 'red');
		action(auto_row, auto_col);
	    }
	}

    }
}



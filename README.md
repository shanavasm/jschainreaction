JsChain-Reaction
================

This is the javascript version of popular android game Chain Reaction.
In addition to supporting 2 to 8 players, this version also supports single
player game.

The 3D grid, spin and split animations will work in IE9+, firefox and chrome.

TODO
=====
To create a online version in which different players can play through the 
network.
